import { DataProvider } from './../../providers/data/data';
import { Word } from './../../model/model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-addword',
  templateUrl: 'addword.html',
})
export class AddwordPage {

  newWordForm: FormGroup;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private formBuilder: FormBuilder, 
    private dataProvider: DataProvider) {
      this.newWordForm = this.formBuilder.group({
        word: [ '', Validators.required ],
        translation: [ '', Validators.required ],
        deff: [ '', Validators.required ]
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddwordPage');
  }

  ionViewDidEnter() {
    this.newWordForm.reset();
  }

  onSave() {
    var newWord = new Word();
    newWord.word = this.newWordForm.controls['word'].value;
    newWord.translation = this.newWordForm.controls['translation'].value;
    newWord.deff = this.newWordForm.controls['deff'].value;

    this.dataProvider.saveWord(newWord);
    this.newWordForm.reset();
  }
}
