import { Word } from './../../model/model';
import { DataProvider } from './../../providers/data/data';
import { UniqueRandomProvider } from './../../providers/unique-random/unique-random';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DEBUG, WORDS } from '../../constants/constants';

const TAG = '===============TestingPage===============';

@IonicPage()
@Component({
  selector: 'page-testing',
  templateUrl: 'testing.html',
})
export class TestingPage {

  word: any;
  words: Array<Word> = [];
  rgValue: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private urnd: UniqueRandomProvider, 
    private dataProvider: DataProvider) {

      this.word = navParams.get('word');

      if (DEBUG) {
        console.log(TAG, 'ionViewDidLoad');
        console.log(TAG, this.word);
      }
  }

  ionViewDidLoad() {
    this.fillWords();
  }

  fillWords() {
    this.dataProvider.getData(WORDS).then(res => {
      if (res) {
        var rnds = this.urnd.getRandoms(0, res.length - 1, 5);
        rnds.forEach(element => {
          this.words.push(res.find(f => f.id === element))
        });

        if (!this.words.some(f => { return f.id == this.word.id})) {
          this.words[0] = this.word;
        } 
        
        this.urnd.shuffleArray(this.words);
       
        if (DEBUG) {
          console.log(TAG, this.words);
        }
      }
    });
  }

  onRadioSelected() {
    if (DEBUG) {
      console.log(TAG, this.rgValue);
    }

    if (this.rgValue == this.word.id) {
      this.dataProvider.incrementWordAnswersCount(this.rgValue);
    }

    this.navCtrl.pop();
  }

}
