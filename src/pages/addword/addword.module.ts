import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddwordPage } from './addword';

@NgModule({
  declarations: [
    AddwordPage,
  ],
  imports: [
    IonicPageModule.forChild(AddwordPage),
  ],
})
export class AddwordPageModule {}
