import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TestingPage } from './testing';
import { UniqueRandomProvider } from '../../providers/unique-random/unique-random';

@NgModule({
  declarations: [
    TestingPage,
  ],
  imports: [
    IonicPageModule.forChild(TestingPage),
  ],
  providers: [
    UniqueRandomProvider
  ]
})
export class TestingPageModule {}
