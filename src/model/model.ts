export class Word {
    public id: number;
    public word: string;
    public translation: string;
    public deff: string;
    public correctimgpath: string;
    public answerscount: number;

    constructor() { }
}