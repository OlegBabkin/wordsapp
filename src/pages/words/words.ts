import { Word } from './../../model/model';
import { DEBUG, WORDS } from './../../constants/constants';
import { DataProvider } from './../../providers/data/data';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

const TAG = '===============WordsPage===============';

@IonicPage()
@Component({
  selector: 'page-words',
  templateUrl: 'words.html',
})
export class WordsPage {

  public words: Array<Word>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private dataProvider: DataProvider) {
  }

  ionViewDidLoad() {
    if (DEBUG) {
      console.log(TAG, 'ionViewDidLoad');
    }
  }

  ionViewDidEnter() {
    if (DEBUG) {
      console.log(TAG, 'ionViewDidEnter');
    }
    this.fetchData();
  }

  fetchData() {
    this.dataProvider.getData(WORDS).then(res => {
      if (res) {
        this.words = res.filter(v => v.answerscount < 5);
        if (DEBUG) {
          console.log(TAG, this.words);
        }
      }
    });
  }

  getWords(searchinput) {

    var query = searchinput.srcElement.value;

    if (!query) {
      this.fetchData();
    }

    this.words = this.words.filter((f) => {
      if (f.word && query) {
        if (f.word.toLowerCase().indexOf(query.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  }

  onWordClick(word) {
    this.navCtrl.push('TestingPage', {
      word: word
    });
  }
}
