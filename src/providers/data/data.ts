import { DEBUG } from './../../constants/constants';
import { Word } from './../../model/model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { WORDS } from '../../constants/constants';

const PATH = 'assets/data.json';
const TAG = '===============DataProvider===============';

@Injectable()
export class DataProvider {

  words: Array<Word> = [];

  constructor(private http: HttpClient, private storage: Storage) {
    storage.get(WORDS).then(res => {
      if (res) {
        this.words = res
      } else {
        this.getWords().subscribe(res => {
          this.words = res;
          this.saveData(WORDS, this.words);
        },
          err => console.log(err),
          () => console.log('json file loaded')
        );
      }
    });

    if (DEBUG) {
      console.log(TAG, this.words);
    }
  }

  private getWords(): Observable<Word[]> {
    return this.http.get<Word[]>(PATH);
  }

  getData(key) {
    return this.storage.get(key);
  }

  saveData(key, data) {
    this.storage.set(key, data);
  }

  saveWord(word) {
    if (word) {
      word.id = this.words.length;
      word.answerscount = 0;
      word.correctimgpath = '';

      if (DEBUG) {
        console.log(TAG, word);
      }

      this.words.push(word);
      this.saveData(WORDS, this.words);
    }
  }

  updateWord(word, index) {
    this.words[index] = word;
    this.saveData(WORDS, this.words);
  }

  incrementWordAnswersCount(index) {
    this.words[index].answerscount++;
    this.saveData(WORDS, this.words);
  }
}
