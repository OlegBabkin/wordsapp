import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KnownwordsPage } from './knownwords';

@NgModule({
  declarations: [
    KnownwordsPage,
  ],
  imports: [
    IonicPageModule.forChild(KnownwordsPage),
  ],
})
export class KnownwordsPageModule {}
