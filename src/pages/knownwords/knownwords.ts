import { WORDS, DEBUG } from './../../constants/constants';
import { DataProvider } from './../../providers/data/data';
import { Word } from './../../model/model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

const TAG = '===============KnownwordsPage===============';

@IonicPage()
@Component({
  selector: 'page-knownwords',
  templateUrl: 'knownwords.html',
})
export class KnownwordsPage {

  words: Array<Word>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private dataProvider: DataProvider) {
  }

  ionViewDidLoad() {
    if (DEBUG) {
      console.log(TAG, 'ionViewDidLoad');
    }
  }

  ionViewDidEnter() {
    if (DEBUG) {
      console.log(TAG, 'ionViewDidEnter');
    }
    this.fetchData();
  }

  fetchData() {
    this.dataProvider.getData(WORDS).then(res => {
      if (res) {
        this.words = res.filter(v => v.answerscount >= 5);
        if (DEBUG) {
          console.log(TAG, this.words);
        }
      }
    });
  }

  getWords(searchinput) {

    var query = searchinput.srcElement.value;

    if (!query) {
      this.fetchData();
    }

    this.words = this.words.filter((f) => {
      if (f.word && query) {
        if (f.word.toLowerCase().indexOf(query.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  }

  onWordClick(word) {
    this.navCtrl.push('DetailPage', {
      word: word
    });
  }

}
