import { Component } from '@angular/core';

import { IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'WordsPage';
  tab2Root = 'AddwordPage';
  tab3Root = 'KnownwordsPage';
  tab4Root = 'AboutPage';

  constructor() {

  }
}
