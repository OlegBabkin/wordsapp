import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class UniqueRandomProvider {

  constructor(public http: HttpClient) { }

  getRandoms(min, max, count) {
    var thrownNumbers = [];
    for (let i = 0; i < count; i++) {
      thrownNumbers.push(this.rand(min, max, thrownNumbers));
    }
    return thrownNumbers;
  }

  getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  shuffleArray(array) {
    var curIndex = array.length, tempValue, randIndex;

    while (curIndex !== 0) {
      randIndex = Math.floor(Math.random() * curIndex);
      curIndex--;

      tempValue = array[curIndex];
      array[curIndex] = array[randIndex];
      array[randIndex] = tempValue;
    }

    return array;
  }

  private rand(min, max, nums) {
    var num = this.getRandom(min, max);
    return nums.find(v => v === num) ? this.rand(min, max, nums) : num;
  }

}
